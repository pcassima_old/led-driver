/*
 * Written by P. Cassiman
 */


// Pin definitions for the driver mosfets/transistors (to be determined)
#define RGB_RED 0
#define RGB_GREEN 0
#define RGB_BLUE 0
// Pin definition for the capacitive switch (to be determined)
#define CAP_SW 0

// Pin definition for the CA/CC selector (to be determined)
#define CA_CC_SELECT 0

// Variables used in the ISR's (volatile)
volatile bool buttonPressed = false;
volatile bool buttonReleased = false;

// General variables
unsigned long buttonPressStart = 0;
unsigned long buttonPressDuration = 0;

void setup() {
  // Define outputs
  pinMode(RGB_RED, OUTPUT);
  pinMode(RGB_GREEN, OUTPUT);
  pinMode(RGB_BLUE, OUTPUT);

  // Define inputs
  pinMode(CAP_SW, INPUT);
  pinMode(CA_CC_SELECT, INPUT);

  // Attach interrupts
  attachInterrupt(digitalPinToInterrupt(CAP_SW), capSwRising, RISING);
  attachInterrupt(digitalPinToInterrupt(CAP_SW), capSwFalling, FALLING);
  

}

void capSwRising() {
  /*
   * Interrupt service routine to run on the rising edge of the switch.
   * Millis is saved here and nothing else is done. This to determine
   * how long the switch has been pressed.
   */
  buttonPressed = true;
}

void capSwFalling() {
  /*
   * Interrupt service routine to run on the falling edge of the switch.
   * Here the time the button is pressed is determined by comparing the
   * millis value from the first ISR, with the current value.
   * This way the program can distinguish between a short and long press.
   */
  buttonReleased = true;
}

void loop() {
  // put your main code here, to run repeatedly:
  if (buttonPressed) {
    // If the button has been pressed, reset the variable and save the current millis.
    buttonPressed = false;
    buttonPressStart = millis();
  }

  if (buttonReleased) {
    /* 
     *  When the button is released, reset the variable and calculate the duration of 
     *  button press. According to this, an appropriate reaction can be taken.
     */
    buttonReleased = false;
    buttonPressDuration = millis() - buttonPressStart; 
  }

  if (250 <= buttonPressDuration <= 750) {
    /*
     * If the button-press lasts between 250 and 750 ms (+- 0,5s), it is a short push.
     */
     // Reset the duration-variable.
     buttonPressDuration = 0;
  }

  else if (1750 <= buttonPressDuration <= 2250) {
    /*
     * If the button-press lasts between 1750 and 2250ms (+- 2s), it is a long push.
     */
     // Reset the duration-variable.
     buttonPressDuration = 0;
  }

  else {
    // change nothing or continue doing the same...
  }

}
