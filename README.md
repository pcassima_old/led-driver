# LED driver

## What is it?
A simple Arduino sketch targetted at the ATtiny85 microcontroller. For use as a RGB-LED driver

Both CA (Common Anode) and CC (Common Cathode) LEDs can be used. This is done by placing the driving transistors/mosfets in a push-pull configuration.
A switch is used to distinguish between CA & CC LEDs

## How does it work?

[To be added]

## Why was it made?

[To be added]

## WIKI

There is a [wiki](https://gitlab.com/pcassima/led-driver/wikis/home "The LED driver wiki") available with all the information related to the LED driver.